# README #
This is a Vim syntax file for the `faded-white.vim` color scheme. 

### How do I get set up? ###
If you don't have a preferred installation method, I recommend installing [pathogen.vim](https://github.com/tpope/vim-pathogen), and then simply copy and paste:

    cd ~/.vim/bundle
    git clone https://JohnKaul@bitbucket.org/JohnKaul/faded-white.vim.git

Otherwise you can always download this and place the `faded-white.vim` file in the `colors` directory. 

### Contribution guidelines ###

* Contribute? Please. Feel free.
* Code review? Yes, please.
* Comments? Yes, please.

### Git Standards ###
Each commit will be structured like this:
`"main.cpp: Formatting cleanup."`
Where the file name is listed first followed by a colon and a brief description of the change.

### Coding Standards ###
1. Each file should contain the line: `// File Last Updated: ` followed by the date. I use a simple vim mapping for this.

### Who do I talk to? ###

* John Kaul - john.kaul@outlook.com
